//
//  GitLabPipelineTestApp.swift
//  GitLabPipelineTest
//
//  Created by Andriy Petrovskyi on 07.02.2023.
//

import SwiftUI

@main
struct GitLabPipelineTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
